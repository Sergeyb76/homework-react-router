import React from "react";

interface INotFoundProps {}

const NotFound: React.FC<INotFoundProps> = () => {
  return (
    <div>
      <div className="card" style={{ width: "40rem" }}>
        <div className="card-body">
          <h5 className="card-title">Page not found</h5>
          Test
        </div>
      </div>
    </div>
  );
};

export default NotFound;
