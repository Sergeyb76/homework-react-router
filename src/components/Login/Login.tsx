import React from "react";
interface ILoginProps {}

const Login: React.FC<ILoginProps> = () => {
  return (
    <div>
      <div className="card" style={{ width: "40rem" }}>
        <div className="card-body">
          <h5 className="card-title">Login</h5>
          <div className="mb-3">
            <label htmlFor="exampleFormControlInput1" className="form-label">
              Name
            </label>
            <input
              type="text"
              className="form-control"
              id="exampleFormControlInput1"
            ></input>
          </div>

          <div className="mb-3">
            <label htmlFor="exampleFormControlInput1" className="form-label">
              Password
            </label>
            <input
              type="password"
              className="form-control"
              id="exampleFormControlInput1"
            ></input>
          </div>
          <button type="button" className="btn btn-primary">
            Login
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
