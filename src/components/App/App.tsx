import React from "react";
import { Route, Routes, NavLink } from "react-router-dom";
import HomePage from "../HomePage";
import Login from "../Login";
import NotFound from "../NotFound";
import Registration from "../Registration";
import "./App.css";

interface IAppProps {}

const App: React.FC<IAppProps> = () => {
  return (
    <div>
      <div className="menu-container">
        <NavLink className="nav-link " to="/login">
          Login
        </NavLink>
        <NavLink className="nav-link " to="/registration">
          Registration
        </NavLink>
        <NavLink className="nav-link " to="/notfound">
          Page not found (404)
        </NavLink>
        <NavLink className="nav-link " to="/home">
          Home
        </NavLink>
      </div>

      <Routes>
        <Route path="*" element={<HomePage />} />
        <Route path="/login" element={<Login />} />
        <Route path="/registration" element={<Registration />} />
        <Route path="/home" element={<HomePage />} />
        <Route path="/notfound" element={<NotFound />} />
      </Routes>
    </div>
  );
};

export default App;
