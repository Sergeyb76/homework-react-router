import React from "react";
import { useAppDispatch, useAppSelector } from "../../Hooks/Redux";
import { PageSlice } from "../../store/Reducers/Slice";

interface IHomePageProps {}

const HomePage: React.FC<IHomePageProps> = () => {
  const dispatch = useAppDispatch();
  const { activePage } = useAppSelector((state) => state.PageSlice);
  const { setActivePage } = PageSlice.actions;

  return (
    <div>
      <div className="card" style={{ width: "40rem" }}>
        <div className="card-body">
          <h5 className="card-title">Home</h5>

          <div
            style={{ display: "flex", flexDirection: "column", width: "190px" }}
          >
            Active page: {activePage}
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => dispatch(setActivePage(activePage + 1))}
            >
              Set active page
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
